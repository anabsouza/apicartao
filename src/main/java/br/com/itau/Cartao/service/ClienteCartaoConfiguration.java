package br.com.itau.Cartao.service;

import br.com.itau.Cartao.gateway.CartaoClienteDecoder;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteCartaoConfiguration {

    @Bean
    public ErrorDecoder getCarClientDecoder() {
        return new CartaoClienteDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CartaoClienteFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}

